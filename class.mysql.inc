<?php

/**
 * @author Heron Santos by TelosOnline.info
 * @copyright 2008
 * @class Conex�o com Banco de Dados 
 */

/**
 * @example
 *  $bancodedados = new MySQL("localhost","3306","root","root","sistema"); //chama a classe MySQL
 *  $bancodedados->SQL("select * from conta");
 */	

$url_check = $_SERVER["PHP_SELF"];
if (eregi("classMySQL.inc", "$url_check")) {
	header ("Location: /index.php");
}

class MySQL {
	/** construtor */
	public function __construct() { }

	/** define as configura��es de acesso */
	public function CREATE($hostname,$port,$username,$password,$name){
		$this->hostname = trim($hostname);
		$this->port = trim($port);
		$this->username = trim($username);
		$this->password = trim($password);
		$this->name = trim($name);
	}
	
	/** abre conex�o com banco de dados */
	public function _OPEN() {
		$this->connection = mysql_connect("$this->hostname:$this->port",$this->username,$this->password);
		if (!$this->connection) {
			echo mysql_error();
			die();
		} elseif (!mysql_select_db($this->name,$this->connection)) {
			echo mysql_error();
			die();
		}
	}
	
	/** fecha conex�o com banco de dados */
	public function _CLOSE() {
		return mysql_close($this->connection);
	}
	
	/** executa rotina em sql */
	public function SQL($value) {
		$this->_OPEN();
		$this->pvalue = trim($value);
		if ($this->result = mysql_query($this->pvalue)) {
			$this->_CLOSE();
			return $this->result;
		} else {
			echo mysql_error();
			die();
		}
	}
	
	/** fecha conex�o com banco de dados */
	public function GET($tabela,$coluna,$condicao = null,$ordenacao = null) {
		$this->_OPEN();
		$this->tabela = trim($tabela);
		$this->coluna = trim($coluna);
		$this->condicao = trim($condicao);
		$this->ordenacao = trim($ordenacao);
		
		if (!empty($this->condicao)) $this->condicao = " where ".$this->condicao;
		if (!empty($this->ordenacao)) $this->ordenacao = " order by ".$this->ordenacao;
		
		if ($this->select = mysql_query("select ".$this->coluna." from ".$this->tabela.$this->condicao.$this->ordenacao)) {
			$this->value = mysql_result($this->select, 0, $this->coluna);
			$this->_CLOSE();
			return $this->value;
		} else {
			echo mysql_error();
			die();
		}
	}
	
	/** fecha conex�o com banco de dados */
	public function SET($tabela,$coluna,$valor,$condicao = null) {
		$this->_OPEN();
		$this->tabela = trim($tabela);
		$this->coluna = trim($coluna);
		$this->valor = trim($valor);
		$this->condicao = trim($condicao);
		
		if (!empty($this->condicao)) $this->condicao = " where ".$this->condicao;

		if ($this->update = mysql_query("update ".$this->tabela." set ".$this->coluna." = ".$this->valor.$this->condicao)) {
			$this->_CLOSE();
		} else {
			echo mysql_error();
			die();
		}
	}
	
	/** retorna o valor do pr�ximo registro */
	public function NEXT($tabela,$coluna) {
		$this->_OPEN();
		$this->tabela = trim($tabela);
		$this->coluna = trim($coluna);
	
		$this->rows = mysql_num_rows(mysql_query("select * from ".$this->tabela));
		if ($this->rows > 0) { // se existe algum registro
			$this->maxrows = mysql_query("select max(".$this->coluna.")+1 as total from ".$this->tabela." where 1");
			while (is_array($record = mysql_fetch_array($this->maxrows))) {
				$this->value = $record["total"];
			}
		} else {
			$this->value = 1;
		}
		
		$this->_CLOSE();
		return $this->value;
	}
	
	/** retorna o total de registros */
	public function COUNT($tabela,$condicao = null) {
		$this->_OPEN();
		$this->tabela = trim($tabela);
		$this->condicao = trim($condicao);
		
		if (!empty($this->condicao)) $this->condicao = " where ".$this->condicao;

		$this->count = mysql_query("select count(*) as total from ".$this->tabela.$this->condicao);
		$this->value = mysql_result($this->count, 0, "total");

		$this->_CLOSE();
		return $this->value;
	}

	
	/** retorna o total de registros */
	public function CHECK($tabela,$condicao = null) {
		$this->_OPEN();
		$this->tabela = trim($tabela);
		$this->condicao = trim($condicao);
		
		if (!empty($this->condicao)) $this->condicao = " where ".$this->condicao;

		$this->count = mysql_query("select count(*) as total from ".$this->tabela.$this->condicao);
		$this->value = mysql_result($this->count, 0, "total");

		$this->_CLOSE();
		if ($this->value == 0) {
			return false;
		} else {
			return true;
		}
	}

	/** retorna o total acumulativo */
	public function SUM($tabela,$coluna,$condicao = null) {
		$this->_OPEN();
		$this->tabela = trim($tabela);
		$this->coluna = trim($coluna);
		$this->condicao = trim($condicao);
		
		if (!empty($this->condicao)) $this->condicao = " where ".$this->condicao;

		$this->sum = mysql_query("select sum(".$this->coluna.") as total from ".$this->tabela.$this->condicao);
		$this->value = mysql_result($this->sum, 0, "total");

		$this->_CLOSE();
		return $this->value;
	}
}
?>