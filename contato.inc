<?php

/**
 * @author Heron R. dos Santos
 * @copyright 2008 TelosOnline.info
 */



?>
<h1>Contato</h1>
<p>Preencha o formul�rio abaixo e aguarde nosso retorno...</p>
<p>Sua opini�o � muito importante para n�s!</p>
<?php
	if ($post->action == "post") {
		if (empty($post->nome) or empty($post->email) or empty($post->assunto) or empty($post->mensagem)) {
			$return_type = "erro";
			$return_title = "Formul�rio Incompleto!";
			$return_caption = "Os campos com * s�o obrigat�rios...";
		} elseif (!checkEmail($post->email)) {
			$return_type = "erro";
			$return_title = "Problemas!";
			$return_caption = "E-mail especificado n�o � v�lido...";
		} else {
			// defini��es interna
			$smtp_name = "Edson Cardoso - Edinho, 65321";
			$smtp_to = 'edscard@gmail.com';
			$smtp_subject = "Fale Conosco, edinho65321.can.br";
			
			// corpo da mensagem
			$body  = "Data: $data.\n";
			$body .= "Nome: ".$str->DBTOSTR($post->nome)." (".$str->DBTOSTR($post->email).").\n";
			$body .= "Assunto: ".$str->DBTOSTR($post->assunto).".\n";
			$body .= "Mensagem: ".$str->DBTOSTR($post->mensagem).".\n";
			
			// monta cabe�alho
			$headers  = "From: \"".$str->DBTOSTR($post->nome)."\" <".$str->DBTOSTR($post->email).">\r\n";
			$headers .= "Reply-To: ".$str->DBTOSTR($post->email)."\n";
			
			// envia e-mail
			if (mail($smtp_to, $smtp_subject, $body, $headers)) {
				// corpo da mensagem
				$body  = "Ol� ".$str->DBTOSTR($post->nome)."!\n";
				$body .= "Confirmamos que a sua mensagem enviada, atrav�s do site www.edinho65321.can.br, foi recebida e que breve estaremos retornando contato.\n";
				$body .= "Atenciosamente,\n";
				$body .= "Edson Cardoso - Edinho, 65321\n";
				$body .= "www.edinho65321.can.br";
				
				// monta cabe�alho
				$headers  = "From: \"$smtp_name\" <$smtp_to>\r\n";
				$headers .= "Reply-To: $smtp_to\n";
				
				// envia e-mail
				if (mail($post->email, $smtp_subject, $body, $headers)) {
					$return_type = "ok";
					$return_title = "Sucesso!";
					$return_caption = "Mensagem enviada com sucesso...";
				} else {
					$return_type = "erro";
					$return_title = "Problemas!";
					$return_caption = "Erro durante o envio da mensagem...";
				}
			} else {
				$return_type = "erro";
				$return_title = "Problemas!";
				$return_caption = "Erro durante o envio da mensagem...";
			}
		}
		echo "<fieldset id=\"box\" class=\"$return_type\">";
		echo "<img class=\"icon\" src=\"images/box.$return_type.gif\">";
		echo "<strong>$return_title</strong><br>";
		echo "$return_caption";
		echo "</fieldset>";
	}
?>
<?php
	if (empty($post->assunto)) $post->assunto = $get->assunto;
?>
<form method="post">
    <input type="hidden" name="action" value="post" />
    <input type="hidden" name="data" value="<?php echo date("d/m/Y") ?>" />
    <fieldset>
        <legend>Pessoais</legend>
        <label for="nome">Nome: <strong style="color: #FF0000;">*</strong></label><input type="text" maxlength="255" name="nome" id="nome" value="<?php echo $post->nome ?>" />
        <label for="email">E-mail: <strong style="color: #FF0000;">*</strong></label><input type="text" maxlength="255" name="email" id="email" value="<?php echo $post->email ?>" />
        <label for="assunto">Assunto: <strong style="color: #FF0000;">*</strong></label><input type="text" maxlength="255" name="assunto" id="assunto" value="<?php echo $post->assunto ?>" />
        <label for="mensagem">Mensagem: <strong style="color: #FF0000;">*</strong></label><textarea name="mensagem" id="mensagem" rows="8"><?php echo $post->mensagem ?></textarea>
    </fieldset>
    <div id="button">
        <input type="submit" value="Ok" name="button" id="button" />
    </div>
</form>