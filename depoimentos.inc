<?php

/**
 * @author Heron R. dos Santos
 * @copyright 2008 TelosOnline.info
 */



?>
<p class="buttonback">
	<a href="?pagina=contato&assunto=Meu Depoimento"><img src="images/icone.mandeseudepoimento.gif"></a>
</p>
<h1>Depoimentos</h1>
<p>
	<img src="images/foto.lindberg.jpg" class="foto-esquerda">
	<i>"Edinho � um companheiro que tem uma ampla hist�ria de luta e envolvimento com Nova Igua�u. Sua atua��o nas �reas de juventude, meio ambiente e desenvolvimento comunit�rio t�m sido sua marca como militante pol�tico. Desejo todo o sucesso a esse jovem ativista"</i><br>
	<span class="assinatura-depoimento">Lindberg Farias</span><br>
	Prefeito de Nova Igua�u.
</p>
<br>
<p>
	<img src="images/foto.randal.jpg" class="foto-direita">
	<i>"Seja no trabalho em programas sociais e de juventude desenvolvido pela Prefeitura, seja pela sua milit�ncia nas lutas populares, Edinho me impressiona pelo seu dinamismo, humanidade e �tica, que o credenciam para ocupar uma vaga na C�mara de Vereadores. Eu confio!"</i><br>
	<span class="assinatura-depoimento">Randal Farah</span><br>
	Secret�rio Municipal de Desenvolvimento Econ�mico e Social da Prefeitura de Nova Igua�u.
</p>
<br>
<p>
	<i>"Realmente precisamos de pessoas como o Edson para q nosso partido continue a evoluir. Acredito fielmente que ele poder� trazer muito mais benef�cios n�o s� para Tr�s Cora��es, mas tamb�m para outros bairros carentes de nossa cidade..."</i><br>
	<span class="assinatura-depoimento">Amalia Albanese</span><br>
	Dirigente do PCdoB - Nova Iguacu.
</p>
<br>
<p>
	<img src="images/foto.isabel.jpg" class="foto-esquerda">
	<i>"Pra voc� da comunidade que deseja renova��o, indico esta joven lideran�a. Edson Cardoso para vereador de Nova Igua�u"</i><br>
	<span class="assinatura-depoimento">Isabel Serra</span><br>
	Presidente da Federa��o das Associa��es de Moradores de Nova Igua�u - MAB.
</p>
<br><br>
<p>
	<img src="images/foto.jandira.jpg" class="foto-direita">
	<i>"Conheci Edinho nas caminhadas pela cidade de Nova Igua�u, na luta por melhores condi��es da sa�de e de saneamento, apesar de muito jovem Edinho tem uma experi�ncia sem igual na pol�tica e precisamos de UM JOVEM EM A��O na C�mara de Vereadores e esse jovem, n�o tenho d�vidas � o Edson Cardoso - Edinho"</i><br>
	<span class="assinatura-depoimento">Jandira Feghali</span><br>
	Candidata � Prefeita da Cidade do Rio de Janeiro.
</p>
<br>
<p>
	<img src="images/foto.cappelli.jpg" class="foto-esquerda">
	<i>"Tive a oportunidade de conhecer, este aguerrido jovem quando cheguei na administra��o em Nova Igua�u, Edinho foi fundamental para entendermos a realidade da juventude igua�uana e desenvolvermos projetos importantes como foi o Juventude Cidad� onde qualificamos 3000 jovens em nossa cidade. Um jovem idealista, lutador e que conhece as verdadeiras necessidades n�o s� da juventude, mas tamb�m para o povo igua�uano. Esse voto vale apena. N�o desperdice seu voto!!!"</i><br>
	<span class="assinatura-depoimento">Ricardo Cappelli</span><br>
	Ex-Secret�rio de Desenvolvimento Econ�mico e Social da Prefeitura de Nova Igua�u.
</p>
<br>
<p>
	<i>"Conheci o Edinho por acaso, a pessoa que conversava com ele n�o era brasileiro. Assuntei e percebi que aquele jovem se preparava para participar de um FORUM MUNDIAL para jovens lideres em suas comunidades. O Edinho, na �poca, era o presidente da AMATRECO. Resumindo, quando o Edson descobriu que eu era professor, de maneira sutil e inteligente, me convenceu a me engajar em um projeto social para ensinar crian�as e jovens carentes. Saltava aos olhos que o Edinho era movido por projetos sociais. O garoto tem o cacoete de empreendedor, ele quer promover a igualdade de oportunidades. Vamos produzir o nosso empreendedor social, vamos ajudar o Edinho..."</i><br>
	<span class="assinatura-depoimento">Ant�nio de Souza Lima</span><br>
	Professor e Visinho de Edson Cardoso - Edinho.
</p>
<br>
<p>
	<img src="images/foto.joao.jpg" class="foto-direita">
	<i>"Conheci Edinho quando iniciamos o nosso curso de Ci�ncias Sociais na UCAM em 2001, na unidade do Instituto de Humanidades. Ajudamos a criar o Diret�rio Acad�mico Herbert de Souza e de quebra constru�mos a nossa amizade. Desde ent�o, permanecemos juntos e, colado em prol de uma Sociedade mais justa e igualit�ria. Edinho � um jovem com id�ias grandiosas e tem um orgulho danado do lugar onde vive. Ali�s, um pol�tico por voca��o. A escolha CERTA para vereador em Nova Igua�u."</i><br>
	<span class="assinatura-depoimento">Jo�o Carlos Farias</span><br>
	Soci�logo e Coordenador de Acessibilidade do IBDD.
</p>
<br>
<p>
	<img src="images/foto.edmilsonvalentim.jpg" class="foto-esquerda">
	<i>"Edinho � um combatente na luta por uma sociedade mais justa. Sua bagagem pol�tica acumulada com a milit�ncia no movimento estudantil e principalmente no movimento comunit�rio o fez conhecer intimamente as necessidades da popula��o de seu munic�pio.Nova Igua�u precisa de vereadores comprometidos com o desenvolvimento e Edinho est� preparado para desempenhar este papel."</i><br>
	<span class="assinatura-depoimento">Edmilson Valentim</span><br>
	Deputado Federal PCdoB/RJ.
</p>
<br>
<p>
	<i>"Decide apoiar o Edinho porque entendi que ele tinha uma vis�o futur�stica para a juventude em especial e tamb�m para toda popula��o de Nova Igua�u, al�m disso, Edinho � um cara de car�ter indiscut�vel por isso amigo acho que para o dia 5 de outubro n�o existe outro igual nesse dia Edson Cardoso Edinho � 65321!!! esse � o voto certo!!"</i><br>
	<span class="assinatura-depoimento">Francisco L�cio</span><br>
	Grupo Jovem da Igreja Quadrangular.
</p>
<br>
<p>
	<i>"Todo e qualquer projeto, independente ao que se destina, precisa de vontade pol�tica. Esse Projeto de inclus�o da Baixada Fluminense ao munic�pio do Rio de Janeiro n�o poderia ser diferente! Para que "aconte�a de verdade", estou delegando ao futuro Vereador Edson Cardoso � Edinho atribui��o na divulga��o e defesa  da implanta��o do tra�ado do TRANSPORTE DE MASSA TIPO METR� para baixada, esse servi�o sobre trilhos interligado a baixada a esta��o Pavuna do metr�, atendendo a quatro munic�pios, inclusive com seis esta��es na cidade de Nova Igua�u, revolucionar� a qualidade de vida da regi�o dentre outros fatores de inser��o quer seja no trabalho, educa��o, com�rcio, ordenamento urbano, ocupa��o de solo, meio ambiente, esporte e lazer. Edinho ser� um defensor desse projeto na c�mara de vereadores."</i><br>
	<span class="assinatura-depoimento">Fernando Mac Dowell</span><br>
	Prof. Dr. e Livre Docente em Engenharia - UFRJ.
</p>