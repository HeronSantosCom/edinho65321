<?php

/**
 * @author Heron R. dos Santos
 * @copyright 2008 TelosOnline.info
 */

	include("configuracao.inc");

?>
<html>

<head>
	<meta http-equiv="content-type" content="text/html; charset=iso-8859-1">
	<meta name="author" content="Heron R. dos Santos">

	<title>Edson Cardoso - Edinho, � 65321!</title>
		
	<style>
		@import "general.css";
		@import "index.css";
	</style>
	
	<script src="javascript.js" type="text/javascript"></script>
	<script src="player/AC_RunActiveContent.js" type="text/javascript"></script>
	<script type="text/javascript">
	function loadConsole() {
		getPopup('popup.htm', 'popup', '200', '200', 'no', 'no', 'no', 'no');
	}
	//loadConsole();
	</script>
</head>

<body>

	<div id="layout">
		<table id="container" cellspacing="0">
			<!-- topo -->
			<tr id="topo">
				<td>
					<table id="divisoria" cellspacing="0">
						<tr>
							<td><a href="http://www.edinho65321.can.br/"><img src="images/topo01.gif"></a></td>
							<td><a href="http://www.edinho65321.can.br/"><img src="images/topo02.gif"></a></td>
						</tr>
						<tr>
							<td><a href="http://www.edinho65321.can.br/"><img src="images/topo03.gif"></a></td>
							<td><a href="http://www.edinho65321.can.br/"><img src="images/topo04.gif"></a></td>
						</tr>
					</table>
				</td>
			</tr>
			
			<!-- menu -->
			<tr id="menu">
				<td>
					<table id="divisoria" cellspacing="0">
						<tr>
							<td><img src="images/topo.menu.lado.esquerdo.gif"></td>
							
							<td><a href="?pagina=perfil"><img src="images/topo.menu.perfil.gif"></a></td>
							<td><a href="?pagina=projetos"><img src="images/topo.menu.projetos.gif"></a></td>
							<td><a href="?pagina=apoiadores"><img src="images/topo.menu.apoiadores.gif"></a></td>
							<td><a href="?pagina=agenda"><img src="images/topo.menu.agenda.gif"></a></td>
							<td><a href="?pagina=noticias"><img src="images/topo.menu.noticias.gif"></a></td>
							<td><a href="?pagina=depoimentos"><img src="images/topo.menu.depoimentos.gif"></a></td>
							<td><a href="?pagina=material"><img src="images/topo.menu.material.gif"></a></td>
							<td><a href="?pagina=contato"><img src="images/topo.menu.contato.gif"></a></td>
							
							<td><img src="images/topo.menu.lado.direito.gif"></td>
						</tr>
					</table>
				</td>
			</tr>
			
			<!-- corpo -->
			<tr id="corpo">
				<td>
					<div id="conteudo">
						<?php
							include($opener);
						?>
					</div>
				</td>
			</tr>

			
			<!-- corpo -->
			<tr id="jingle">
				<td>
					<div id="conteudo">
						<?php
							// logon efetuado com sucesso
							if($autenticado and $module_name != "logoff") {
						?>
							<?php
								$return_type = "warning";
								$return_title = "Autenticado!";
								$return_caption = "Voc� encontra-se autenticado no sistema como administrador...";
							?>
							<fieldset id="box" class="<?php echo $return_type ?>">
								<img class="icon" src="images/box.<?php echo $return_type ?>.gif">
								<strong><?php echo $return_title ?></strong><br>
								<?php echo $return_caption ?>
							    <div id="button">
							        <input type="button" value="Efetuar Logoff" name="button" id="button" onClick="getOpen('?pagina=logoff','_self')" />
							    </div>
							</fieldset>
						<?php
							}
						?>
					<?php
						if ($module_name != "default") {
							echo "<p class=\"buttonback\"><a href=\"?pagina=default\"><img src=\"images/icone.paginaprincipal.gif\"></a></p>";
						} else {
							echo "<p class=\"player\"><embed type=\"application/x-shockwave-flash\" src=\"player/player.swf\" style=\"\" id=\"player\" name=\"player\" bgcolor=\"#ffffff\" quality=\"high\" allowfullscreen=\"false\" allowscriptaccess=\"always\" wmode=\"opaque\" flashvars=\"file=material/jingle.mp3&amp;autostart=true\" width=\"100%\" height=\"20\"></p>";
						}
						if ($module_name == "depoimentos") {
							echo "<p class=\"buttonback\"><a href=\"?pagina=contato&assunto=Meu Depoimento\"><img src=\"images/icone.mandeseudepoimento.gif\"></a></p>";
						}
					?>
					</div>
				</td>
			</tr>
			
			<!-- rodape -->
			<tr id="rodape">
				<td>
					<table id="divisoria" cellspacing="0">
						<tr>
							<td><img src="images/fundo.rodape.gif"></td>
							<td><a href="http://www.telosonline.info/" target="_blank"><img src="images/fundo.telosonline.gif"></a></td>
						</tr>
						<tr>
							<td><img src="images/fundo.copyright.gif"></td>
							<td><a href="http://www.flickr.com/photos/advancevisual" target="_blank"><img src="images/fundo.visualadvance.gif"></a></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>	
	</div>
	
	<script type="text/javascript">
		//loadConsole();
	</script>
	
	<script type="text/javascript">
		var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
		document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
	</script>
	<script type="text/javascript">
		var pageTracker = _gat._getTracker("UA-5231030-4");
		pageTracker._trackPageview();
	</script>
</body>
</html>