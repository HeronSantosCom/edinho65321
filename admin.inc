<?php

/**
 * @author Heron R. dos Santos
 * @copyright 2008 TelosOnline.info
 */



?>
<h1>Administra��o</h1>
<?php
	// logon efetuado com sucesso
	if($autenticado) {
?>
	<meta http-equiv='refresh' content='1;url=index.php?pagina=default'>
	<?php
		$return_type = "ok";
		$return_title = "Sucesso!";
		$return_caption = "Autenticado com sucesso!<br>Aguarde o redirecionamento...";
	?>
	<fieldset id="box" class="<?php echo $return_type ?>">
		<img class="icon" src="images/box.<?php echo $return_type ?>.gif">
		<strong><?php echo $return_title ?></strong><br>
		<?php echo $return_caption ?>
	</fieldset>
<?php
	// logon expirou
	} elseif($erro) {
?>
	<?php
		$return_type = "erro";
		$return_title = "Problemas!";
		$return_caption = "N�o foi poss�vel autenticar-se, prov�vel que o usu�rio e a senha estejam incorretos...<br>Clique em \"Tentar novamente...\" para prosseguir...";
	?>
	<fieldset id="box" class="<?php echo $return_type ?>">
		<img class="icon" src="images/box.<?php echo $return_type ?>.gif">
		<strong><?php echo $return_title ?></strong><br>
		<?php echo $return_caption ?>
	    <div id="button">
	        <input type="button" value="Tentar novamente..." name="button" id="button" onClick="getOpen('?pagina=admin','_self')" />
	    </div>
	</fieldset>
<?php
	// exibe a tela principal
	} else {
?>
	<form method="post">
	    <input type="hidden" name="action" value="logon" />
	    <fieldset>
	        <legend>Autenticar-se</legend>
	        <label for="username">Usu�rio: <strong style="color: #FF0000;">*</strong></label><input type="text" value="" name="username" id="username" />
	        <label for="password">Senha: <strong style="color: #FF0000;">*</strong></label><input type="password" value="" name="password" id="password" />
	    </fieldset>
	    <div id="button">
	        <input type="button" value="Cancelar" name="button" id="button" onClick="getOpen('?','_self')"  />
	        <input type="submit" value="Ok" name="button" id="button" />
	    </div>
	</form>
<?php
	}
?>