<?php

/**
 * @author Heron R. dos Santos
 * @copyright 2008 TelosOnline.info
 */

	session_start();
	//ini_set("display_errors", 0 );
	//error_reporting(0);

/**
 * Carrega M�dulo
 */
	$module_name = $_GET["pagina"];
	if (empty($module_name)) $module_name = "default";
	if (!fopen("$module_name.inc","r")) {
		$module_name = "erro";
	}
	$opener = "$module_name.inc";

/**
 * Carrega Banco de Dados
 */
	require_once("class.mysql.inc");
	$db = new MySQL;
	$db_host = "localhost";
	$db_port = 3306;
	$db_user = "root";
	$db_pass = "";
	$db_name = "edinho65_db";
	$db->CREATE($db_host,$db_port,$db_user,$db_pass,$db_name);

/**
 * Carrega String
 */
	require_once("class.string.inc");
	$str = new String;

/**
 * Resquest
 */
	class NullObject{};
	$get = new NullObject();
	foreach ($_GET as $k => $v){
		$get->$k = $str->STRTODB($v);
	}

	$post = new NullObject();
	foreach ($_POST as $k => $v){
		$post->$k = $str->STRTODB($v);
	}

	$session = new NullObject();
	foreach ($_POST as $k => $v){
		$session->$k = $str->STRTODB($v);
	}

/**
 * Validador de Email
 */
	function checkEmail($email) {
		$mail_correcto = false;
		//verifico umas coisas
		if ((strlen($email) >= 6) && (substr_count($email,"@") == 1) && (substr($email,0,1) != "@") && (substr($email,strlen($email)-1,1) != "@")) {
			if ((!strstr($email,"'")) && (!strstr($email,"\"")) && (!strstr($email,"\\")) && (!strstr($email,"\$")) && (!strstr($email," "))) {
				//vejo se tem caracter .
				if (substr_count($email,".")>= 1) {
					//obtenho a termina��o do dominio
					$term_dom = substr(strrchr ($email, '.'),1);
					//verifico que a termina��o do dominio seja correcta
					if (strlen($term_dom)>1 && strlen($term_dom)<5 && (!strstr($term_dom,"@")) ) {
						//verifico que o de antes do dominio seja correcto
						$antes_dom = substr($email,0,strlen($email) - strlen($term_dom) - 1);
						$caracter_ult = substr($antes_dom,strlen($antes_dom)-1,1);
						if ($caracter_ult != "@" && $caracter_ult != ".") {
							$mail_correcto = true;
						}
					}
				}
			}
		}
		if ($mail_correcto) return true; else return false; 
	} 

/**
 * Validador de Data
 */	
	function checkData($date) {
		if (!isset($date) || $date=="") {
			return false;
		}
		list($dd,$mm,$yy)=explode("/",$date);
		if ($dd!="" && $mm!="" && $yy!="") {
			return checkdate($mm,$dd,$yy);
		}
		return false;
	}

/**
 * Validador de Hora
 */
	function checkHora($time) {
		if(ereg("^([0-1][0-9]|[2][0-3]):[0-5][0-9]$", $time)){
  			return true;
		} else {
			return false;
		}
	}

/**
 * Login
 */
	// efetua login
	if($post->action == "logon") {
		if ($post->username == "admin" and $post->password == "4697bae7") {
			$_SESSION["username"] = $post->username;
			$_SESSION["password"] = $post->password;
			$_SESSION["logon"] = true;
			$erro = false;
		} else {	
			unset($_SESSION["username"]);
			unset($_SESSION["password"]);
			$_SESSION["logon"] = false;
			$erro = true;
		}
	}
	
	if($erro) {
		$autenticado = false;
	} else {
		if ($_SESSION["logon"]) {
			$autenticado = true;
		} else {
			$autenticado = false;
		}
	}

/**
 * Defini��o de Cabe�alho
 */
	header("Content-Type: text/html; charset=iso-8859-1",true);
	
?>