<?php

/**
 * @author Heron R. dos Santos
 * @copyright 2008 TelosOnline.info
 */

	unset($_SESSION["username"]);
	unset($_SESSION["password"]);
	
	$_SESSION["logon"] = false;

?>
<h1>Efetuando Logoff</h1>
<?php
	$return_type = "ok";
	$return_title = "Sucesso!";
	$return_caption = "Logoff efetuado com sucesso...";
?>
<fieldset id="box" class="<?php echo $return_type ?>">
	<img class="icon" src="images/box.<?php echo $return_type ?>.gif">
	<strong><?php echo $return_title ?></strong><br>
	<?php echo $return_caption ?>
    <div id="button">
        <input type="button" value="Ok" name="button" id="button" onClick="getOpen('?','_self')" />
    </div>
</fieldset>