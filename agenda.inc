<?php

/**
 * @author Heron R. dos Santos
 * @copyright 2008 TelosOnline.info
 */



?>
<h1>Agenda</h1>
<?php
	// se submetido formulario
	if ($post->action == "post") {
		// se N�O definido ID por POST
		if (empty($post->id)) {
			// verifica dados
			if (empty($post->data) or empty($post->hora) or empty($post->titulo) or empty($post->descricao) or empty($post->local)) {
				$return_type = "erro";
				$return_title = "Formul�rio Incompleto!";
				$return_caption = "Os campos com * s�o obrigat�rios...";
			// checa data
			} elseif (!checkData($post->data) or !checkHora($post->hora)) {
				$return_type = "erro";
				$return_title = "Problemas!";
				$return_caption = "A data ou hora especificada n�o � v�lida...";
			// checa existencia de evento por data e hora
			} elseif ($db->CHECK("agenda","data = \"".$str->DATE($post->data,"DD/MM/YYYY","YYYY-MM-DD")."\" and hora = \"$post->hora\"")) {
				$return_type = "erro";
				$return_title = "Problemas!";
				$return_caption = "J� existe um evento no hor�rio indicado...";
			// checa existencia de evento por titulo e local
			} elseif ($db->CHECK("agenda","titulo = \"$post->titulo\" and local = \"$post->local\"")) {
				$return_type = "erro";
				$return_title = "Problemas!";
				$return_caption = "O evento j� existe...";
			// caso contr�rio insere
			} else {
				$insert_agenda = "insert into agenda set
					data = \"".$str->DATE($post->data,"DD/MM/YYYY","YYYY-MM-DD")."\",
					hora = \"".$post->hora."\",
					titulo = \"".$post->titulo."\",
					descricao = \"".$post->descricao."\",
					local = \"".$post->local."\"";
				$db->SQL($insert_agenda);
				$return_type = "ok";
				$return_title = "Sucesso!";
				$return_caption = "O evento foi cadastrado...";
			}
		// se definido ID verifica se existe
		} elseif ($db->CHECK("agenda","id = $post->id")) {
			// verifica dados
			if (empty($post->data) or empty($post->hora) or empty($post->titulo) or empty($post->descricao) or empty($post->local)) {
				$return_type = "erro";
				$return_title = "Formul�rio Incompleto!";
				$return_caption = "Os campos com * s�o obrigat�rios...";
			// checa data
			} elseif (!checkData($post->data) or !checkHora($post->hora)) {
				$return_type = "erro";
				$return_title = "Problemas!";
				$return_caption = "A data ou hora especificada n�o � v�lida...";
			// checa existencia de evento por data e hora
			} elseif (($post->data != $post->data_atual or $post->hora != $post->hora_atual) and ($db->CHECK("agenda","data = \"".$str->DATE($post->data,"DD/MM/YYYY","YYYY-MM-DD")."\" and hora = \"$post->hora\""))) {
					$return_type = "erro";
					$return_title = "Problemas!";
					$return_caption = "J� existe um evento no hor�rio indicado...";
			// checa existencia de evento por titulo e local
			} elseif (($post->titulo != $post->titulo_atual or $post->local != $post->local_atual) and $db->CHECK("agenda","titulo = \"$post->titulo\" and local = \"$post->local\"")) {
				$return_type = "erro";
				$return_title = "Problemas!";
				$return_caption = "O evento j� existe...";
			// caso contr�rio atualiza
			} else {
				$update_agenda = "update agenda set
					data = \"".$str->DATE($post->data,"DD/MM/YYYY","YYYY-MM-DD")."\",
					hora = \"".$post->hora."\",
					titulo = \"".$post->titulo."\",
					descricao = \"".$post->descricao."\",
					local = \"".$post->local."\"
					where id = $post->id";
				$db->SQL($update_agenda);
				$return_type = "ok";
				$return_title = "Sucesso!";
				$return_caption = "O evento foi alterado...";
			}
		}
		echo "<fieldset id=\"box\" class=\"$return_type\">";
		echo "<img class=\"icon\" src=\"images/box.$return_type.gif\">";
		echo "<strong>$return_title</strong><br>";
		echo "$return_caption";
		echo "</fieldset>";
	}
?>
<?php
	if ($autenticado and ($get->action == "atualizar" or $get->action == "inserir") and $return_type != "ok") {
?>
	<?php
		// se definido ID por GET
		if (!empty($get->id)){
			// verifica se ID existe
			if ($db->CHECK("agenda","id=$get->id")) {
				// carrega os dados
				$select_agenda = "select * from agenda where id = \"$get->id\"";
				$agenda = mysql_fetch_array($db->SQL($select_agenda));
				// grava os dados nas variaveis
				foreach ($agenda as $k => $v){
					$post->$k = $str->DBTOSTR($v);
				}
				// converte data
				$post->data = $str->DATE($post->data,"YYYY-MM-DD","DD/MM/YYYY");
			//sen�o
			} else {
				$return_type = "erro";
				$return_title = "Problemas!";
				$return_caption = "O evento n�o foi encontrado...";
				echo "<fieldset id=\"box\" class=\"$return_type\">";
				echo "<img class=\"icon\" src=\"images/box.$return_type.gif\">";
				echo "<strong>$return_title</strong><br>";
				echo "$return_caption";
				echo "</fieldset>";
			}
		}
	?>
	<form method="post">
	    <input type="hidden" name="action" value="post" />
	    <input type="hidden" name="id" value="<?php echo $post->id ?>" />
	    <fieldset>
	        <legend>Cadastrar na Agenda</legend>
	        <label for="data">Data: <strong style="color: #FF0000;">*</strong></label><input type="text" maxlength="10" name="data" id="data" value="<?php echo $post->data ?>" style="width: 15%;" onkeypress="return MaskData(this, event)" />
	        <label for="hora">Hora: <strong style="color: #FF0000;">*</strong></label><input type="text" maxlength="8" name="hora" id="hora" value="<?php echo $post->hora ?>" style="width: 10%;" onkeypress="return MaskHora(this, event)" />
	        <label for="titulo">Titulo: <strong style="color: #FF0000;">*</strong></label><input type="text" maxlength="255" name="titulo" id="titulo" value="<?php echo $post->titulo ?>" />
	        <label for="descricao">Descri��o: <strong style="color: #FF0000;">*</strong></label><textarea name="descricao" id="descricao" rows="8"><?php echo $post->descricao ?></textarea>
	        <label for="local">Local: <strong style="color: #FF0000;">*</strong></label><input type="text" maxlength="255" name="local" id="local" value="<?php echo $post->local ?>" />
	    </fieldset>
	    
	    <input type="hidden" name="data_atual" value="<?php echo $post->data ?>" />
	    <input type="hidden" name="hora_atual" value="<?php echo $post->hora ?>" />
	    <input type="hidden" name="titulo_atual" value="<?php echo $post->titulo ?>" />
	    <input type="hidden" name="local_atual" value="<?php echo $post->local ?>" />
	    
	    <div id="button">
	        <input type="button" value="Cancelar" name="button" id="button" onClick="getOpen('?pagina=agenda','_self')"  />
	        <input type="submit" value="Ok" name="button" id="button" />
	    </div>
	</form>
<?php
	// se removendo
	} elseif ($autenticado and $get->action == "remover") {
?>
	<?php
		// se com id e n�o confirmado
		if (!empty($get->id) and !$get->confirm){
			if ($db->CHECK("agenda","id=$get->id")) {
				// carrega os dados
				$select_agenda = "select id,titulo from agenda where id = \"$get->id\"";
				$agenda = mysql_fetch_array($db->SQL($select_agenda));
				// grava os dados nas variaveis
				foreach ($agenda as $k => $v){
					$post->$k = $str->DBTOSTR($v);
				}
				$return_type = "question";
				$return_title = "Remover Evento?";
				$return_caption = "Deseja remover o evento \"$post->titulo\"?";
				echo "<fieldset id=\"box\" class=\"<?php echo $return_type ?>\">";
				echo "<img class=\"icon\" src=\"images/box.$return_type.gif\">";
				echo "<strong>$return_title</strong><br>";
				echo "$return_caption";
				echo "<div id=\"button\">";
				echo "<input type=\"button\" value=\"Sim\" name=\"button\" id=\"button\" onClick=\"getOpen('?pagina=agenda&action=remover&confirm=true&id=$post->id','_self')\" />";
				echo "<input type=\"button\" value=\"N�o\" name=\"button\" id=\"button\" onClick=\"getOpen('?pagina=agenda','_self')\" />";
				echo "</div>";
				echo "</fieldset>";
			//sen�o
			} else {
				$return_type = "erro";
				$return_title = "Problemas!";
				$return_caption = "O evento n�o foi encontrado...";
				echo "<fieldset id=\"box\" class=\"$return_type\">";
				echo "<img class=\"icon\" src=\"images/box.$return_type.gif\">";
				echo "<strong>$return_title</strong><br>";
				echo "$return_caption";
				echo "</fieldset>";
			}
		// se com id e confirmado
		} elseif (!empty($get->id) and $get->confirm) {
			// carrega os dados
			$select_agenda = "select id from agenda where id = \"$get->id\"";
			$agenda = mysql_fetch_array($db->SQL($select_agenda));
			// grava os dados nas variaveis
			foreach ($agenda as $k => $v){
				$post->$k = $str->DBTOSTR($v);
			}
			$return_type = "ok";
			$return_title = "Sucesso!";
			$return_caption = "O evento foi removido com sucesso...";
			echo "<fieldset id=\"box\" class=\"$return_type\">";
			echo "<img class=\"icon\" src=\"images/box.$return_type.gif\">";
			echo "<strong>$return_title</strong><br>";
			echo "$return_caption";
			echo "<div id=\"button\">";
			echo "<input type=\"button\" value=\"Ok\" name=\"button\" id=\"button\" onClick=\"getOpen('?pagina=agenda','_self')\" />";
			echo "</div>";
			echo "</fieldset>";
			$delete_agenda = "delete from agenda where id = ". $get->id;
			$db->SQL($delete_agenda);
		}
	?>
<?php
	// senao lista eventos
	} else {
?>
	<?php
		// se autenticado
		if ($autenticado) {
			// n�o define condicao
			$condicao = "";
		} else {
			// define condicao de data maior ou igual a atual
			$condicao = "data >= \"".date("Y-m-d")."\"";
		}
		// se existir evento
		if ($db->CHECK("agenda",$condicao)) {
	?>
		<?php
			// se autenticado
			if ($autenticado) {
				// carrega eventos por data no total
				$select_agenda_data = $db->SQL("select data from agenda group by data order by data asc");
			} else {
				// carrega eventos por data maior ou igual a atual
				$select_agenda_data = $db->SQL("select data from agenda where data >= \"".date("Y-m-d")."\" group by data order by data asc");
			}
			while ($agenda_data = mysql_fetch_array($select_agenda_data)) {
				$data_atual = $agenda_data["data"];
				echo "<u><h4>".$str->DATE($data_atual,"YYYY-MM-DD","DD/MM/YYYY")."</h4></u>";
				// carregas os eventos de acordo com a data a ser exibida
				$select_agenda = $db->SQL("select * from agenda where data = \"$data_atual\" order by hora asc");
				while ($agenda = mysql_fetch_array($select_agenda)) {
					$id = $agenda["id"];
					$hora = $agenda["hora"];
					$titulo = $agenda["titulo"];
					$descricao = $agenda["descricao"];
					$local = $agenda["local"];
					echo "<p>";
					echo "<span class=\"agenda-titulo\">$titulo</span><br>";
					echo "<i>$descricao</i><br>";
					echo "Ocorrer� no(a) <strong><i>$local</i></strong> �s <strong><i>$hora</i></strong>.";
					if ($autenticado) {
						echo "<br>(<strong><a href=\"?pagina=agenda&id=$id&action=atualizar\">ALTERAR</a> - <a href=\"?pagina=agenda&id=$id&action=remover\">REMOVER</a>)</strong>";
					}
					echo "</p>\n";
				}
			}
		?>
	<?php
		} else {
	?>
		<?php
			$return_type = "warning";
			$return_title = "Aguarde!";
			$return_caption = "Em breve, estarei disponibilizando minha agenda para voc� acompanhar nossos movimentos dentro do munic�pio...";
		?>
		<fieldset id="box" class="<?php echo $return_type ?>">
			<img class="icon" src="images/box.<?php echo $return_type ?>.gif">
			<strong><?php echo $return_title ?></strong><br>
			<?php echo $return_caption ?>
		</fieldset>
	<?php
		}
	?>
	<?php
		if ($autenticado) {
		    echo "<div id=\"button\">";
		    echo "<input type=\"button\" value=\"Cadastrar\" name=\"button\" id=\"button\" onClick=\"getOpen('?pagina=agenda&action=inserir','_self')\"  />";
		    echo "</div>";
		}
	?>
<?php
	}
?>