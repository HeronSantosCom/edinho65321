<?php

/**
 * @author Heron R. dos Santos
 * @copyright 2008 TelosOnline.info
 */



?>
<h1>Not�cias</h1>
<?php
	// se submetido formulario
	if ($post->action == "post") {
		// se N�O definido ID por POST
		if (empty($post->id)) {
			// verifica dados
			if (empty($post->titulo) or empty($post->texto)) {
				$return_type = "erro";
				$return_title = "Formul�rio Incompleto!";
				$return_caption = "Os campos com * s�o obrigat�rios...";
			// checa data
			} elseif ($db->CHECK("noticia","titulo LIKE \"%$post->titulo%\"")) {
				$return_type = "erro";
				$return_title = "Problemas!";
				$return_caption = "A not�cia j� existe ou algo semelhante...";
			// caso contr�rio insere
			} else {
				//gera o nome do arquivo
				$post->arquivo = md5(date("Y-m-d") . date("H:i:s") . $post->titulo);
				
				//insere no banco de dados
				$insert_noticia = "insert into noticia set
					data = \"".date("Y-m-d")."\",
					hora = \"".date("H:i:s")."\",
					titulo = \"".$post->titulo."\",
					arquivo = \"".$post->arquivo."\"";
				$db->SQL($insert_noticia);
				$return_type = "ok";
				$return_title = "Sucesso!";
				$return_caption = "A not�cia foi cadastrada...";
				
				$filename = "noticias/".$post->arquivo.".db";
				$newfile = fopen($filename, "a-");
				fwrite($newfile, $post->texto);
				fclose($newfile);
			}
		// se definido ID verifica se existe
		} elseif ($db->CHECK("noticia","id = $post->id")) {
			// verifica dados
			if (empty($post->titulo) or empty($post->texto)) {
				$return_type = "erro";
				$return_title = "Formul�rio Incompleto!";
				$return_caption = "Os campos com * s�o obrigat�rios...";
			// checa data
			} elseif (($post->titulo != $post->titulo_tmp) and $db->CHECK("noticia","titulo like \"%$post->titulo%\"")) {
				$return_type = "erro";
				$return_title = "Problemas!";
				$return_caption = "A not�cia j� existe...";
			// caso contr�rio atualiza
			} else {
				$update_noticia = "update noticia set
					titulo = \"".$post->titulo."\"
					where id = $post->id";
				$db->SQL($update_noticia);
				$return_type = "ok";
				$return_title = "Sucesso!";
				$return_caption = "O evento foi alterado...";
				
				$filename = "noticias/".$post->arquivo.".db";
				$newfile = fopen($filename, "w");
				fwrite($newfile, $post->texto);
				fclose($newfile);
			}
		}
		echo "<fieldset id=\"box\" class=\"$return_type\">";
		echo "<img class=\"icon\" src=\"images/box.$return_type.gif\">";
		echo "<strong>$return_title</strong><br>";
		echo "$return_caption";
		echo "</fieldset>";
	}
?>
<?php
	if ($autenticado and ($get->action == "atualizar" or $get->action == "inserir") and $return_type != "ok") {
?>
	<?php
		// se definido ID por GET
		if (!empty($get->id)){
			// verifica se ID existe
			if ($db->CHECK("noticia","id=$get->id")) {
				// carrega os dados
				$select_noticia = "select * from noticia where id = \"$get->id\"";
				$noticia = mysql_fetch_array($db->SQL($select_noticia));
				// grava os dados nas variaveis
				foreach ($noticia as $k => $v){
					$post->$k = $str->DBTOSTR($v);
				}
				
				// l� o conte�do do arquivo para uma string
				$filename = "noticias/".$post->arquivo.".db";
				$openfile = fopen($filename, "r");
				$post->texto =  fread($openfile, filesize ($filename));
				fclose($openfile);
			//sen�o
			} else {
				$return_type = "erro";
				$return_title = "Problemas!";
				$return_caption = "A not�cia n�o foi encontrada...";
				echo "<fieldset id=\"box\" class=\"$return_type\">";
				echo "<img class=\"icon\" src=\"images/box.$return_type.gif\">";
				echo "<strong>$return_title</strong><br>";
				echo "$return_caption";
				echo "</fieldset>";
			}
		}
	?>
	<head>
	<!-- TinyMCE -->
	<script type="text/javascript" src="editor/tiny_mce.js"></script>
	<script language="javascript" type="text/javascript">
		tinyMCE.init({
			mode : "textareas",
			language : "pt",
			theme : "simple"
		});
	</script>
	<!-- /TinyMCE -->
	</head>
	<form method="post">
	    <input type="hidden" name="action" value="post" />
	    <input type="hidden" name="id" value="<?php echo $post->id ?>" />
	    <input type="hidden" name="arquivo" value="<?php echo $post->arquivo ?>" />
	    <fieldset>
	        <legend>Not�cia</legend>
	        <label for="titulo">T�tulo: <strong style="color: #FF0000;">*</strong></label><input type="text" maxlength="255" name="titulo" id="titulo" value="<?php echo $post->titulo ?>" />
	        <label for="texto">Texto: <strong style="color: #FF0000;">*</strong></label><textarea id="texto" name="texto" rows="15" style="width: 100%"><?php echo $post->texto ?> </textarea>
	    </fieldset>
	    
	    <input type="hidden" name="titulo_tmp" value="<?php echo $post->titulo ?>" />
	    
	    <div id="button">
	        <input type="button" value="Cancelar" name="button" id="button" onClick="getOpen('?pagina=noticias','_self')"  />
	        <input type="submit" value="Ok" name="button" id="button" />
	    </div>
	</form>
<?php
	} elseif ($autenticado and $get->action == "remover") {
?>
	<?php
		if (!empty($get->id) and !$get->confirm){
			if ($db->CHECK("noticia","id=$get->id")) {
				// carrega os dados
				$select_noticia = "select id,titulo from noticia where id = \"$get->id\"";
				$noticia = mysql_fetch_array($db->SQL($select_noticia));
				// grava os dados nas variaveis
				foreach ($noticia as $k => $v){
					$post->$k = $str->DBTOSTR($v);
				}
				
				$return_type = "question";
				$return_title = "Remover Evento?";
				$return_caption = "Deseja remover a not�cia \"$post->titulo\"?";
				echo "<fieldset id=\"box\" class=\"<?php echo $return_type ?>\">";
				echo "<img class=\"icon\" src=\"images/box.$return_type.gif\">";
				echo "<strong>$return_title</strong><br>";
				echo "$return_caption";
				echo "<div id=\"button\">";
				echo "<input type=\"button\" value=\"Sim\" name=\"button\" id=\"button\" onClick=\"getOpen('?pagina=noticias&action=remover&confirm=true&id=$post->id','_self')\" />";
				echo "<input type=\"button\" value=\"N�o\" name=\"button\" id=\"button\" onClick=\"getOpen('?pagina=noticias','_self')\" />";
				echo "</div>";
				echo "</fieldset>";
			//sen�o
			} else {
				$return_type = "erro";
				$return_title = "Problemas!";
				$return_caption = "A not�cia n�o foi encontrada...";
				echo "<fieldset id=\"box\" class=\"$return_type\">";
				echo "<img class=\"icon\" src=\"images/box.$return_type.gif\">";
				echo "<strong>$return_title</strong><br>";
				echo "$return_caption";
				echo "</fieldset>";
			}
		} elseif (!empty($get->id) and $get->confirm) {
			// carrega os dados
			$select_noticia = "select id,arquivo from noticia where id = \"$get->id\"";
			$noticia = mysql_fetch_array($db->SQL($select_noticia));
			// grava os dados nas variaveis
			foreach ($noticia as $k => $v){
				$post->$k = $str->DBTOSTR($v);
			}
			
			$return_type = "ok";
			$return_title = "Sucesso!";
			$return_caption = "A not�cia foi removida com sucesso...";
			echo "<fieldset id=\"box\" class=\"$return_type\">";
			echo "<img class=\"icon\" src=\"images/box.$return_type.gif\">";
			echo "<strong>$return_title</strong><br>";
			echo "$return_caption";
			echo "<div id=\"button\">";
			echo "<input type=\"button\" value=\"Ok\" name=\"button\" id=\"button\" onClick=\"getOpen('?pagina=noticias','_self')\" />";
			echo "</div>";
			echo "</fieldset>";
			
			$filename = "noticias/".$post->arquivo.".db";
			$db->SQL("delete from noticia where id = ". $get->id);
			unlink($filename);
		}
	?>
<?php
	} else {
?>
	<?php
		if ($db->CHECK("noticia")) {
	?>
		<?php
			$record_count = $db->COUNT("noticia");
			
			$sql_limit = 5;
		
			$paginacao = $get->pg;
			if (!isset($paginacao)) {
				$paginacao = 1;
			}
			
			$sql_limit_init = ($paginacao-1) * $sql_limit;
	
			$exibe = ceil($record_count/$sql_limit);
			$exibe++;
	
			$select_noticia = $db->SQL("select * from noticia order by data desc,hora desc limit $sql_limit_init, $sql_limit");
			while ($noticia = mysql_fetch_array($select_noticia)) {
				$id = $str->DBTOSTR($noticia["id"]);
				$data = $str->DATE($noticia["data"],"YYYY-MM-DD","DD/MM/YYYY");
				$hora = $str->DBTOSTR($noticia["hora"]);
				$titulo = $str->DBTOSTR($noticia["titulo"]);
				$arquivo = $str->DBTOSTR($noticia["arquivo"]);

				// l� o conte�do do arquivo para uma string
				$filename = "noticias/".$arquivo.".db";
				$openfile = fopen($filename, "r");
				$texto =  fread($openfile, filesize ($filename));
				fclose($openfile);
				
				echo "<h4>".$titulo."<br><span class=\"noticia-detalhe\">Postado em $data as $hora</span></h4>";
				echo $str->DBTOSTR($texto);
				
				if ($autenticado) {
					echo "<div id=\"button\">";
					echo "<input type=\"button\" value=\"Editar\" name=\"button\" id=\"button\" onClick=\"getOpen('?pagina=noticias&id=$id&action=atualizar','_self')\"  />";
					echo "<input type=\"button\" value=\"Remover\" name=\"button\" id=\"button\" onClick=\"getOpen('?pagina=noticias&id=$id&action=remover','_self')\"  />";
					echo "</div>";
				}
			}
		?>
		<p class="noticias-paginacao">
			<?php 
				if ($paginacao > 1) {
					echo "<a href=\"?pagina=noticias&pg=".($paginacao-1)."\"><strong>&lt; p&aacute;gina anterior</strong></a>";
				} else {
					echo "&lt; p&aacute;gina anterior";
				}
				
				echo " | ";

				$painel = "";
				for ($x=1; $x<=$exibe; $x++) {
					if ($x>=$paginacao-5 && $x<=$paginacao-1) {
						$painel .= " <a href=\"?pagina=noticias&pg=".$x."\">$x</a> ";
					}
					if ($x==$paginacao) {
						$painel .= " <strong>[$x]</strong> ";
					}
					if ($x+1<=$exibe && ($x>=$paginacao+1 && $x<=$paginacao+5)) {
						$painel .= " <a href=\"?pagina=noticias&pg=".$x."\">$x</a> ";
					}
				}
				echo $painel;

				echo " | ";

				if (($paginacao+1) < $exibe) {
					echo "<a href=\"?pagina=noticias&pg=".($paginacao+1)."\"><strong>p&aacute;gina seguinte &gt;</strong></a>";
				} else {
					echo "p&aacute;gina seguinte &gt;";
				}
			?>
		</p>
	<?php
		} else {
	?>
		<?php
			$return_type = "warning";
			$return_title = "Aguarde!";
			$return_caption = "Em breve, disponibilizaremos not�cias das nossas movimenta��es entre outras...";
		?>
		<fieldset id="box" class="<?php echo $return_type ?>">
			<img class="icon" src="images/box.<?php echo $return_type ?>.gif">
			<strong><?php echo $return_title ?></strong><br>
			<?php echo $return_caption ?>
		</fieldset>
	<?php
		}
	?>
	<?php
		if ($autenticado) {
		    echo "<div id=\"button\">";
		    echo "<input type=\"button\" value=\"Cadastrar\" name=\"button\" id=\"button\" onClick=\"getOpen('?pagina=noticias&action=inserir','_self')\"  />";
		    echo "</div>";
		}
	?>
<?php
	}
?>