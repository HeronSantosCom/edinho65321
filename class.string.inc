<?php

/**
 * @author Heron Santos by TelosOnline.info
 * @copyright 2008
 * @class Manipula��o de String 
 */

/**
 * @example
 *  $string = new String(); //chama a classe String
 *  $string->STRTODB("texto");
 */	

$url_check = $_SERVER["PHP_SELF"];
if (eregi("classString.inc", "$url_check")) {
	header ("Location: /index.php");
}

class String {
	/** construtor */
	public function __construct() { }

	/** remove campos html de formularios */
	public function _UNHTMLENTITIES($valor) {
		$this->valor = trim($valor);
		$trans_tbl = get_html_translation_table(HTML_ENTITIES);
		$trans_tbl = array_flip($trans_tbl);
		return strtr($this->valor,$trans_tbl);
	}
	
	
	/** converte os textos para o banco de dados */
	public function DBTOSTR($valor) {
		$this->valor = trim($valor);
		$this->valor = stripslashes($this->valor);
		$this->valor = html_entity_decode($this->valor);
		return $this->valor;
	}

	/** converte os textos para o banco de dados */
	public function STRTODB($valor) {
		$this->valor = $valor;
		// remove palavras que contenham sintaxe sql
        $this->valor = preg_replace("/(from|alter table|select|insert|delete|update|where|drop table|show tables|#|\*|--|\\\\)/i","",$this->valor);
        //limpa espa�os vazio
		$this->valor = trim($this->valor);
        //converte tags html e php
        $this->valor = htmlentities($this->valor);
        //Adiciona barras invertidas a uma string
		$this->valor = addslashes($this->valor);
        return $this->valor;
	}
	
	/** metodo que retorna a data formatada,parametros:data a ser formatada,formato da data atual,formato posterior */ 
	public function DATE($data,$atual,$posterior){ 
		$atual = strtoupper($atual);
		$posterior = strtoupper($posterior); 
		$dia = substr($data,strpos($atual,"DD"),2); 
		$mes = substr($data,strpos($atual,"MM"),2); 
		$ano = substr($data,strpos($atual,"YYYY"),4); 
		$dataNova = str_replace("DD",$dia,$posterior); 
		$dataNova = str_replace("MM",$mes,$dataNova); 
		$dataNova = str_replace("YYYY",$ano,$dataNova); 
		return $dataNova; 
	} 
}
?>