<?php

/**
 * @author Heron R. dos Santos
 * @copyright 2008 TelosOnline.info
 */



?>
<h1>Material de Campanha</h1>
<?php
	$return_type = "warning";
	$return_title = "Instru��es!";
	$return_caption = "Para baixar, pressione com o bot�o direito do mouse e clique em \"Salvar link como...\"!";
?>
<fieldset id="box" class="<?php echo $return_type ?>">
	<img class="icon" src="images/box.<?php echo $return_type ?>.gif">
	<strong><?php echo $return_title ?></strong><br>
	<?php echo $return_caption ?>
</fieldset>
<p>
	<a href="material/panfletojuventude.frente_15x21.zip">
		<img src="images/material.panfletojuventude.frente_15x21.jpg" class="foto-esquerda"><br>
		<span class="jingle-material">Panfleto Juventude</span><br>
		<i>Frente, 15x21.</i>
	</a>
</p>
<br>
<p>
	<a href="material/panfletojuventude.verso_15x21.zip">
		<img src="images/material.panfletojuventude.verso_15x21.jpg" class="foto-esquerda"><br>
		<span class="jingle-material">Panfleto Juventude</span><br>
		<i>Verso, 15x21.</i>
	</a>
</p>
<br>
<p>
	<a href="material/praguinha.zip">
		<img src="images/material.praguinha.jpg" class="foto-esquerda"><br>
		<span class="jingle-material">Praginha</span><br>
	</a>
</p>
<br>
<p>
	<a href="material/santinho.frente_10x15.zip">
		<img src="images/material.santinho.frente_10x15.jpg" class="foto-esquerda"><br>
		<span class="jingle-material">Santinho</span><br>
		<i>Frente, 10x15</i><br><br>
	</a>
</p>
<br>
<p>
	<a href="material/santinho.verso_10x15.zip">
		<img src="images/material.santinho.verso_10x15.jpg" class="foto-esquerda"><br>
		<span class="jingle-material">Santinho</span><br>
		<i>Verso, 10x15</i><br><br>
	</a>
</p>
<br>
<p>
	<a href="material/wallpaper_800x600.zip">
		<img src="images/material.wallpaper_800x600.jpg" class="foto-esquerda"><br>
		<span class="jingle-material">Papel de Parede (wallpaper)</span><br>
		<i>800x600</i>
	</a>
</p>
<br>
<p>
	<a href="material/wallpaper_1024x768.zip">
		<img src="images/material.wallpaper_1024x768.jpg" class="foto-esquerda"><br>
		<span class="jingle-material">Papel de Parede (wallpaper)</span><br>
		<i>1024x768</i>
	</a>
</p>
<br>
<p>
	<a href="material/jingle.mp3">
		<img src="images/material.jingle.gif" class="foto-esquerda"><br>
		<span class="jingle-material">Jingle da Campanha</span><br>
		<i>MC Suelen - Edson Cardoso, 65321.</i>
	</a>
</p>
<p>
	<span class="jingle-material">Letra</span>
	<p>
		Ele faz e nunca corre, <br>
		tem o cora��o aberto, <br>
		dia 5 de outubro <br>
		todos juntos voto certo.
	</p>
	
	<p>
		Dia 5 de outubro <br>
		come�a a revolu��o, <br>
		Partido PCdoB <br>
		todo jovem em a��o.
	</p>
	
	<p>
		Dignidade para mim, <br>
		tranquilidade pro meu filho, <br>
		� com Edson Cardoso <br>
		abrindo novos caminhos.
	</p>
	
	<p>
		Nem melhor e nem pior <br>
		� um cidad�o comum <br>
		pra ajudar a comunidade 65321
	</p>
	
	<p>
		65321 partido PCdoB <br>
		� o Edson Cardoso que vem pra fortalecer
	</p>
	
	<p>
		esgoto, agua tratada <br>
		asfalto vou te dizer,<br>
		direito de todos n�s <br>
		uma �rea de lazer
	</p>
	
	<p>
		o Edinho saiu pra rua <br>
		mostrando sua proposta <br>
		com raz�o e atitude <br>
		surge a nova esperanca <br>
		juventude se abraca e � hora da mudan�a.
	</p>
	
	<p>
		65321 Partido PCdoB <br>
		� o Edson Cardoso que vem pra fortalecer <br>
		� o Edson Cardoso <br>
		65321, Partido PCdoB
	</p>
</p>