// JavaScript Document

// corre��o de navega��o
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

// mascara de formul�rio
function inMask(objeto, evt, mask) {
	var LetrasU = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	var LetrasL = 'abcdefghijklmnopqrstuvwxyz';
	var Letras  = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
	var Numeros = '0123456789';
	var Fixos  = '().-:/ ';
	var Charset = " !\"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_/`abcdefghijklmnopqrstuvwxyz{|}~";
	
	evt = (evt) ? evt : (window.event) ? window.event : "";
	var value = objeto.value;
	if (evt) {
		var ntecla = (evt.which) ? evt.which : evt.keyCode;
		tecla = Charset.substr(ntecla - 32, 1);
		if (ntecla < 32) return true;
		
		var tamanho = value.length;
		if (tamanho >= mask.length) return false;
		
		var pos = mask.substr(tamanho,1);
		while (Fixos.indexOf(pos) != -1) {
			value += pos;
			tamanho = value.length;
			if (tamanho >= mask.length) return false;
			pos = mask.substr(tamanho,1);
		}
		
		switch (pos) {
			case '#' : if (Numeros.indexOf(tecla) == -1) return false; break;
			case 'A' : if (LetrasU.indexOf(tecla) == -1) return false; break;
			case 'a' : if (LetrasL.indexOf(tecla) == -1) return false; break;
			case 'Z' : if (Letras.indexOf(tecla) == -1) return false; break;
			case '*' : objeto.value = value; return true; break;
			default : return false; break;
		}
	}
	objeto.value = value;
	return true;
}
function MaskCEP(objeto, evt) {
	return inMask(objeto, evt, '##.###-###');
}
function MaskTelefone(objeto, evt) {
	return inMask(objeto, evt, '(##) ####-####');
}
function MaskCPF(objeto, evt) {
	return inMask(objeto, evt, '###.###.###-##');
}
function MaskPlacaCarro(objeto, evt) {
	return inMask(objeto, evt, 'AAA-####');
}
function MaskData(objeto, evt) {
	return inMask(objeto, evt, '##/##/####');
}
function MaskHora(objeto, evt) {
	return inMask(objeto, evt, '##:##');
}
function MaskNumFour(objeto, evt) {
	return inMask(objeto, evt, '####');
}

//open
function getOpen(url_page,url_target){
	window.open(url_page, url_target);
}

//abre popup
function getPopup(endereco, janela, largura, altura, fullscreen, scrollbars, status, resizable) {
	var esquerda = ((screen.width - largura) / 2);
	var topo = ((screen.height - altura) / 2) - 50;
	window.open(endereco, janela, 'width='+largura+', height='+altura+', top='+topo+', left='+esquerda+', fullscreen='+fullscreen+', scrollbars='+scrollbars+', status='+status+', resizable='+resizable);
}