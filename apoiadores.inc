<?php

/**
 * @author Heron R. dos Santos
 * @copyright 2008 TelosOnline.info
 */



?>
<h1>Seja um apoiador!</h1>
<?php
	// se submetido formulario
	if ($post->action == "post") {
		// se N�O definido ID por POST
		if (empty($post->id)) {
			// cria identificador
			$post->identificador = md5(date("Y-m-d").date("H:i:s").$post->nome);
			// verifica campos nulos
			if (empty($post->nome) or empty($post->endereco) or empty($post->bairro) or empty($post->zona_eleitoral) or empty($post->local_votacao) or empty($post->questao1)) {
				$return_type = "erro";
				$return_title = "Formul�rio Incompleto!";
				$return_caption = "Os campos com * s�o obrigat�rios...";
			// checa data
			} elseif (!empty($post->email) and !checkEmail($post->email)) {
				$return_type = "erro";
				$return_title = "Problemas!";
				$return_caption = "E-mail inv�lido...";
			// checa existencia de evento por data e hora
			} elseif (!empty($post->msn) and !checkEmail($post->msn)) {
				$return_type = "erro";
				$return_title = "Problemas!";
				$return_caption = "MSN inv�lido...";
			// checa existencia de evento por data e hora
			} elseif ($db->CHECK("apoiador","nome = \"$post->nome\"")) {
				$return_type = "erro";
				$return_title = "Problemas!";
				$return_caption = "Este nome j� est� cadastrado...";
			// caso contr�rio insere
			} else {
				$insert_apoiador = "insert into apoiador set
					nome = \"".$post->nome."\",
					endereco = \"".$post->bairro."\",
					bairro = \"".$post->bairro."\",
					zona_eleitoral = \"".$post->zona_eleitoral."\",
					secao = \"".$post->secao."\",
					local_votacao = \"".$post->local_votacao."\",
					email = \"".$post->email."\",
					msn = \"".$post->msn."\",
					orkut = \"".$post->orkut."\",
					telefone = \"".$post->telefone."\",
					questao1 = \"".$post->questao1."\",
					identificador = \"".$post->identificador."\",
					data = \"".date("Y-m-d")."\"";
				// se iserido apoiador
				if ($apoiador = $db->SQL($insert_apoiador)) {
					// lista tipos de veiculos
					$select_lst_veiculo = $db->SQL("select * from lst_veiculo");
					while ($lst_veiculo = mysql_fetch_array($select_lst_veiculo)) {
						$id = $lst_veiculo["id"];
						$nome = $lst_veiculo["nome"];
						$lst_veiculo = $str->STRTODB($_POST["lst_veiculo$id"]);
						// se veiculo marcado
						if ($lst_veiculo) {
							// insere item marcado
							$insert_apoiador_lst_veiculo = "insert into apoiador_lst_veiculo set
								identificador_apoiador = \"".$post->identificador."\",
								id_lst_veiculo = \"".$id."\"";
							$db->SQL($insert_apoiador_lst_veiculo);
						}
					}
					//lista forma de ajuda
					$select_lst_ajuda = $db->SQL("select * from lst_ajuda");
					while ($lst_ajuda = mysql_fetch_array($select_lst_ajuda)) {
						$id = $lst_ajuda["id"];
						$nome = $lst_ajuda["nome"];
						$lst_ajuda_adicional = $str->STRTODB($_POST["lst_ajuda_adicional$id"]);
						// se n�o estiver marcado mais estiver respondido a pergunta adicional
						if (!empty($lst_ajuda_adicional) and !$_POST["lst_ajuda$id"]) {
							$_POST["lst_ajuda$id"] = $id;
						}
						$lst_ajuda = $str->STRTODB($_POST["lst_ajuda$id"]);
						// se marcado insere item marcado
						if ($lst_ajuda) {
							$insert_apoiador_lst_ajuda = "insert into apoiador_lst_ajuda set
								identificador_apoiador = \"".$post->identificador."\",
								id_lst_ajuda = \"".$id."\",
								adicional_resposta_lst_ajuda = \"".$lst_ajuda_adicional."\"";
							$db->SQL($insert_apoiador_lst_ajuda);
						}
					}
				}
				$return_type = "ok";
				$return_title = "Sucesso!";
				$return_caption = "Voc� foi cadastrado com sucesso!<br>Aguarde que breve entraremos em contato...";
			}
		// se definido ID verifica se existe
		} elseif ($db->CHECK("apoiador","id = $post->id")) {
			// carrega identificador
			$post->identificador = $db->GET("apoiador","identificador","id = $post->id");
			// verifica campos nulos
			if (empty($post->nome) or empty($post->endereco) or empty($post->bairro) or empty($post->zona_eleitoral) or empty($post->local_votacao) or empty($post->questao1)) {
				$return_type = "erro";
				$return_title = "Formul�rio Incompleto!";
				$return_caption = "Os campos com * s�o obrigat�rios...";
			// checa data
			} elseif (!empty($post->email) and !checkEmail($post->email)) {
				$return_type = "erro";
				$return_title = "Problemas!";
				$return_caption = "E-mail inv�lido...";
			// checa existencia de evento por data e hora
			} elseif (!empty($post->msn) and !checkEmail($post->msn)) {
				$return_type = "erro";
				$return_title = "Problemas!";
				$return_caption = "MSN inv�lido...";
			// checa existencia de evento por data e hora
			} elseif (($post->nome != $post->nome_atual) and $db->CHECK("apoiador","nome = \"$post->nome\"")) {
				$return_type = "erro";
				$return_title = "Problemas!";
				$return_caption = "Este nome j� est� cadastrado...";
			// caso contr�rio insere
			} else {
				$update_apoiador = "update apoiador set
					nome = \"".$post->nome."\",
					endereco = \"".$post->bairro."\",
					bairro = \"".$post->bairro."\",
					zona_eleitoral = \"".$post->zona_eleitoral."\",
					secao = \"".$post->secao."\",
					local_votacao = \"".$post->local_votacao."\",
					email = \"".$post->email."\",
					msn = \"".$post->msn."\",
					orkut = \"".$post->orkut."\",
					telefone = \"".$post->telefone."\",
					questao1 = \"".$post->questao1."\",
					identificador = \"".$post->identificador."\",
					data = \"".date("Y-m-d")."\"
					where id = \"".$post->id."\"";
				// se atualizado apoiador
				if ($apoiador = $db->SQL($update_apoiador)) {
					// lista tipos de veiculos
					$select_lst_veiculo = $db->SQL("select * from lst_veiculo");
					while ($lst_veiculo = mysql_fetch_array($select_lst_veiculo)) {
						$id = $lst_veiculo["id"];
						$nome = $lst_veiculo["nome"];
						$lst_veiculo = $str->STRTODB($_POST["lst_veiculo$id"]);
						// se item estiver inserido
						if ($db->CHECK("apoiador_lst_veiculo","identificador_apoiador = \"$post->identificador\" and id_lst_veiculo = \"$id\"")) {
							// e n�o estiver marcado
							if (!$lst_veiculo) {
								// deleta item
								$delete_apoiador_lst_veiculo = "delete from apoiador_lst_veiculo 
									where identificador_apoiador = \"$post->identificador\" and id_lst_veiculo = \"$id\"";
								$db->SQL($delete_apoiador_lst_veiculo);
							}
						// se o item n�o estiver inserido
						} else {
							// e estiver marcado
							if ($lst_veiculo) {
								// insere item marcado
								$insert_apoiador_lst_veiculo = "insert into apoiador_lst_veiculo set
									identificador_apoiador = \"".$post->identificador."\",
									id_lst_veiculo = \"".$id."\"";
								$db->SQL($insert_apoiador_lst_veiculo);
							}
						}
					}
					//lista forma de ajuda
					$select_lst_ajuda = $db->SQL("select * from lst_ajuda");
					while ($lst_ajuda = mysql_fetch_array($select_lst_ajuda)) {
						$id = $lst_ajuda["id"];
						$nome = $lst_ajuda["nome"];
						$lst_ajuda_adicional = $str->STRTODB($_POST["lst_ajuda_adicional$id"]);
						// se item estiver inserido
						if ($db->CHECK("apoiador_lst_ajuda","identificador_apoiador = \"$post->identificador\" and id_lst_ajuda = \"$id\"")) {
							$lst_ajuda = $str->STRTODB($_POST["lst_ajuda$id"]);
							// e n�o estiver marcado deleta item
							if (!$lst_ajuda) {
								$delete_apoiador_lst_ajuda = "delete from apoiador_lst_ajuda 
									where identificador_apoiador = \"$post->identificador\" and id_lst_ajuda = \"$id\"";
								$db->SQL($delete_apoiador_lst_ajuda);
							}
						// se o item n�o estiver inserido
						} else {
							// se n�o estiver marcado mais estiver respondido a pergunta adicional
							if (!empty($lst_ajuda_adicional) and !$_POST["lst_ajuda$id"]) {
								$_POST["lst_ajuda$id"] = $id;
							}
							$lst_ajuda = $str->STRTODB($_POST["lst_ajuda$id"]);
							// se marcado insere item marcado
							if ($lst_ajuda) {
								$insert_apoiador_lst_ajuda = "insert into apoiador_lst_ajuda set
									identificador_apoiador = \"".$post->identificador."\",
									id_lst_ajuda = \"".$id."\",
									adicional_resposta_lst_ajuda = \"".$lst_ajuda_adicional."\"";
								$db->SQL($insert_apoiador_lst_ajuda);
							}
						}
					}
				}
				$return_type = "ok";
				$return_title = "Sucesso!";
				$return_caption = "O cadastro foi alterado...";
			}
		}
		echo "<fieldset id=\"box\" class=\"$return_type\">";
		echo "<img class=\"icon\" src=\"images/box.$return_type.gif\">";
		echo "<strong>$return_title</strong><br>";
		echo "$return_caption";
	    // se estiver cadastrando e retornar ok
		if ($return_type == "ok" and !$autenticado) {
			echo "<div id=\"button\">";
		    echo "<input type=\"button\" value=\"Ok\" name=\"button\" id=\"button\" onClick=\"getOpen('?pagina=apoiadores','_self')\"  />";
		    echo "</div>";
		}
		echo "</fieldset>";
	}
?>
<?php
	// se autenticado e atualizando, ou inserindo e n�o concluir
	if ((!$autenticado or $get->action == "atualizar" or $get->action == "inserir") and $return_type != "ok") {
?>
	<?php
		// se definido ID por GET
		if (!empty($get->id)){
			// verifica se ID existe
			if ($db->CHECK("apoiador","id=$get->id")) {
				// carrega os dados
				$select_apoiador = "select * from apoiador where id = \"$get->id\"";
				$apoiador = mysql_fetch_array($db->SQL($select_apoiador));
				// grava os dados nas variaveis
				foreach ($apoiador as $k => $v){
					$post->$k = $str->DBTOSTR($v);
				}
				// verifica se existem itens marcano na lista veiculos
				if ($db->CHECK("apoiador_lst_veiculo","identificador_apoiador = \"$post->identificador\"")) {
					// marca os itens encontrados
					$select_apoiador_lst_veiculo = $db->SQL("select id_lst_veiculo from apoiador_lst_veiculo where identificador_apoiador = \"$post->identificador\"");
					while ($apoiador_lst_veiculo = mysql_fetch_array($select_apoiador_lst_veiculo)) {
						$id_lst_veiculo = $apoiador_lst_veiculo["id_lst_veiculo"];
						$_POST["lst_veiculo$id_lst_veiculo"] = $id_lst_veiculo;
					}
				}
				// verifica se existem itens marcano na lista ajuda
				if ($db->CHECK("apoiador_lst_ajuda","identificador_apoiador = \"$post->identificador\"")) {
					// marca os itens encontrados
					$select_apoiador_lst_ajuda = $db->SQL("select id_lst_ajuda,adicional_resposta_lst_ajuda from apoiador_lst_ajuda where identificador_apoiador = \"$post->identificador\"");
					while ($apoiador_lst_ajuda = mysql_fetch_array($select_apoiador_lst_ajuda)) {
						$id_lst_ajuda = $apoiador_lst_ajuda["id_lst_ajuda"];
						$adicional_resposta_lst_ajuda = $apoiador_lst_ajuda["adicional_resposta_lst_ajuda"];
						$_POST["lst_ajuda$id_lst_ajuda"] = $id_lst_ajuda;
						if (!empty($adicional_resposta_lst_ajuda)) {
							$_POST["lst_ajuda_adicional$id_lst_ajuda"] = $adicional_resposta_lst_ajuda;
						}
					}
				}
			//sen�o
			} else {
				$return_type = "erro";
				$return_title = "Problemas!";
				$return_caption = "O cadastro n�o foi encontrado...";
				echo "<fieldset id=\"box\" class=\"$return_type\">";
				echo "<img class=\"icon\" src=\"images/box.$return_type.gif\">";
				echo "<strong>$return_title</strong><br>";
				echo "$return_caption";
				echo "</fieldset>";
			}
		}
	?>
	<?php
		$return_type = "warning";
		$return_title = "Seja um apoiador!";
		$return_caption = "Voc� pode se cadastrar como um dos nossos apoiadores...<br>Basta preencher o formul�rio abaixo, e aguardar nosso contato!";
	?>
	<fieldset id="box" class="<?php echo $return_type ?>">
		<img class="icon" src="images/box.<?php echo $return_type ?>.gif">
		<strong><?php echo $return_title ?></strong><br>
		<?php echo $return_caption ?>
	</fieldset>
		<script type="text/javascript">
			function checkEnable(e, id){
				try{ var element = e.target } catch(er) {};
				try{ var element = event.srcElement } catch(er) {};
				
				if (element.checked) {
					document.getElementById(id).disabled = false;
					document.getElementById(id).focus();
				} else {
					document.getElementById(id).disabled = true;
				}
				
			}
		</script>
		<form method="post" name="apoiadores">
		    <input type="hidden" name="action" value="post" />
		    <input type="hidden" name="id" value="<?php echo $post->id ?>" />
		    <fieldset>
		        <legend>Apoiador</legend>
		        <label for="nome">Nome: <strong style="color: #FF0000;">*</strong></label><input type="text" maxlength="255" name="nome" id="nome" value="<?php echo $post->nome ?>" />
		        <label for="endereco">Endere�o: <strong style="color: #FF0000;">*</strong></label><input type="text" maxlength="255" name="endereco" id="endereco" value="<?php echo $post->endereco ?>" />
		        <label for="bairro">Bairro: <strong style="color: #FF0000;">*</strong></label><input type="text" maxlength="255" name="bairro" id="bairro" value="<?php echo $post->bairro ?>" />
		        <label for="zona_eleitoral">Zona Eleitoral: <strong style="color: #FF0000;">*</strong></label><input type="text" maxlength="255" name="zona_eleitoral" id="zona_eleitoral" value="<?php echo $post->zona_eleitoral ?>" onkeypress="return MaskNumFour(this, event)" />
		        <label for="secao">Se��o: </label><input type="text" maxlength="255" name="secao" id="secao" value="<?php echo $post->secao ?>" onkeypress="return MaskNumFour(this, event)" />
		        <label for="local_votacao">Local de Vota��o: <strong style="color: #FF0000;">*</strong></label><input type="text" maxlength="255" name="local_votacao" id="local_votacao" value="<?php echo $post->local_votacao ?>" />
		        <label for="email">E-mail: </label><input type="text" maxlength="255" name="email" id="email" value="<?php echo $post->email ?>" />
		        <label for="msn">MSN: </label><input type="text" maxlength="255" name="msn" id="msn" value="<?php echo $post->msn ?>" />
		        <label for="orkut">Orkut: </label><input type="text" maxlength="255" name="orkut" id="orkut" value="<?php echo $post->orkut ?>" />
		        <label for="telefone">Telefone: </label><input type="text" maxlength="255" name="telefone" id="telefone" value="<?php echo $post->telefone ?>" onkeypress="return MaskTelefone(this, event)" />
				<label>Ve�culo(s) que possui:</label>
				<?php
					// lista tipos de veiculos
					$select_lst_veiculo = $db->SQL("select * from lst_veiculo");
					while ($lst_veiculo = mysql_fetch_array($select_lst_veiculo)) {
						$id = $lst_veiculo["id"];
						$nome = $lst_veiculo["nome"];
						// desmarca variavel $marcado
						unset($marcado);
						// se item marcado, marca campo
						if ($lst_veiculo["marcado"] == 1) $marcado = "checked=\"checked\"";
						if ($_POST["lst_veiculo$id"]) $marcado = "checked=\"checked\"";
						// exibe campo
						echo "<input type=\"checkbox\" value=\"$id\" title=\"$nome\" id=\"lst_veiculo$id\" name=\"lst_veiculo$id\" class=\"checkbox\" $marcado> $nome<br>";
					}
				?>
				<label for="questao1">Se voc� fosse vereador(a), o que faria por Nova Igua�u? <strong style="color: #FF0000;">*</strong></label><textarea id="questao1" name="questao1" rows="3" style="width: 100%"><?php echo $post->questao1 ?></textarea>
				<label>Vou ajudar:</label>
				<?php
					// lista ajuda
					$select_lst_ajuda = $db->SQL("select * from lst_ajuda");
					while ($lst_ajuda = mysql_fetch_array($select_lst_ajuda)) {
						$id = $lst_ajuda["id"];
						$nome = $lst_ajuda["nome"];
						$adicional_habilidado = $lst_ajuda["adicional_habilidado"];
						$adicional_nome = $lst_ajuda["adicional_nome"];
						// desmarca variavel $marcado
						unset($marcado);
						// se item marcado, marca campo
						if ($lst_ajuda["marcado"] == 1) $marcado = "checked=\"checked\"";
						if ($_POST["lst_ajuda$id"]) $marcado = "checked=\"checked\"";
						// exibe campo
						echo "<input type=\"checkbox\" value=\"$id\" title=\"$nome\" id=\"lst_ajuda$id\" name=\"lst_ajuda$id\" onclick=\"checkEnable(event, 'lst_ajuda_adicional$id')\" class=\"checkbox\" $marcado> $nome<br>";
						// se existe pergunta adicional, exibe
						if ($lst_ajuda["adicional_habilidado"] == 1) {
							if (empty($_POST["lst_ajuda_adicional$id"])) $desabilitado = "disabled=\"disabled\"";
							echo "<label for=\"lst_ajuda_adicional$id\">$adicional_nome: </label><input type=\"text\" maxlength=\"255\" name=\"lst_ajuda_adicional$id\" id=\"lst_ajuda_adicional$id\" value=\"".$_POST["lst_ajuda_adicional$id"]."\" $desabilitado />";
						}
					}
				?>
			</fieldset>
			
		    <input type="hidden" name="nome_atual" value="<?php echo $post->nome ?>" />
		    
		    <div id="button">
		        <input type="button" value="Cancelar" name="button" id="button" onClick="getOpen('?pagina=apoiadores','_self')"  />
		        <input type="submit" value="Ok" name="button" id="button" />
		    </div>
		</form>
<?php
	// se autenticado e removendo
	} elseif ($autenticado and $get->action == "remover") {
?>
	<?php
		// se n�o confirmado e com identificacao
		if (!empty($get->id) and !$get->confirm){
			if ($db->CHECK("apoiador","id=$get->id")) {
				// carrega os dados
				$select_apoiador = "select id,nome from apoiador where id = \"$get->id\"";
				$apoiador = mysql_fetch_array($db->SQL($select_apoiador));
				// grava os dados nas variaveis
				foreach ($apoiador as $k => $v){
					$post->$k = $str->DBTOSTR($v);
				}
				$return_type = "question";
				$return_title = "Remover Evento?";
				$return_caption = "Deseja remover o apoiador \"$post->nome\"?";
				echo "<fieldset id=\"box\" class=\"<?php echo $return_type ?>\">";
				echo "<img class=\"icon\" src=\"images/box.$return_type.gif\">";
				echo "<strong>$return_title</strong><br>";
				echo "$return_caption";
				echo "<div id=\"button\">";
				echo "<input type=\"button\" value=\"Sim\" name=\"button\" id=\"button\" onClick=\"getOpen('?pagina=apoiadores&action=remover&confirm=true&id=$post->id','_self')\" />";
				echo "<input type=\"button\" value=\"N�o\" name=\"button\" id=\"button\" onClick=\"getOpen('?pagina=apoiadores','_self')\" />";
				echo "</div>";
				echo "</fieldset>";
			// exibe erro
			} else {
				$return_type = "erro";
				$return_title = "Problemas!";
				$return_caption = "O evento n�o foi encontrado...";
				echo "<fieldset id=\"box\" class=\"$return_type\">";
				echo "<img class=\"icon\" src=\"images/box.$return_type.gif\">";
				echo "<strong>$return_title</strong><br>";
				echo "$return_caption";
				echo "</fieldset>";
			}
		// se identificado e confirmado
		} elseif (!empty($get->id) and $get->confirm) {
			// carrega os dados
			$select_apoiador = "select id,identificador from apoiador where id = \"$get->id\"";
			$apoiador = mysql_fetch_array($db->SQL($select_apoiador));
			// grava os dados nas variaveis
			foreach ($apoiador as $k => $v){
				$post->$k = $str->DBTOSTR($v);
			}
			$return_type = "ok";
			$return_title = "Sucesso!";
			$return_caption = "O cadastro foi removido com sucesso...";
			echo "<fieldset id=\"box\" class=\"$return_type\">";
			echo "<img class=\"icon\" src=\"images/box.$return_type.gif\">";
			echo "<strong>$return_title</strong><br>";
			echo "$return_caption";
			echo "<div id=\"button\">";
			echo "<input type=\"button\" value=\"Ok\" name=\"button\" id=\"button\" onClick=\"getOpen('?pagina=apoiadores','_self')\" />";
			echo "</div>";
			echo "</fieldset>";
			// remove itens do apoiador
			$delete_apoiador_lst_veiculo = "delete from apoiador_lst_veiculo where identificador_apoiador = \"$post->identificador\"";
			$db->SQL($delete_apoiador_lst_veiculo);
			$delete_apoiador_lst_ajuda = "delete from apoiador_lst_ajuda where identificador_apoiador = \"$post->identificador\"";
			$db->SQL($delete_apoiador_lst_ajuda);
			// removo apoiador
			$delete_apoiador = "delete from apoiador where id = \"$get->id\"";
			$db->SQL($delete_apoiador);
		}
	?>
<?php
	// sen�o, se autenticado
	} elseif ($autenticado) {
?>
	<?php
		// exibe noticias
		if ($db->CHECK("apoiador")) {
	?>
		<?php
			// lista as datas de cadastramento
			$select_apoiador_data = $db->SQL("select data from apoiador group by data order by data desc");
			while ($apoiador_data = mysql_fetch_array($select_apoiador_data)) {
				$data_atual = $apoiador_data["data"];
				echo "<u><h3>".$str->DATE($data_atual,"YYYY-MM-DD","DD/MM/YYYY")."</h3></u>";
				echo "<ul>";
				// lista nome dos apoiadores
				$select_apoiador = $db->SQL("select * from apoiador where data = \"$data_atual\" order by nome asc");
				while ($apoiador = mysql_fetch_array($select_apoiador)) {
					$id = $str->DBTOSTR($apoiador["id"]);
					$nome = $str->DBTOSTR($apoiador["nome"]);
					echo "<li>(<strong><a href=\"?pagina=apoiadores&id=$id&action=atualizar\">ALTERAR</a> - <a href=\"?pagina=apoiadores&id=$id&action=remover\">REMOVER</a></strong>) ".$nome."</li>";
				}
				echo "</ul>\n";
			}
		?>
	<?php
		} else {
	?>
		<?php
			$return_type = "warning";
			$return_title = "Ops!";
			$return_caption = "Nenhum apoiador cadastrado...";
		?>
		<fieldset id="box" class="<?php echo $return_type ?>">
			<img class="icon" src="images/box.<?php echo $return_type ?>.gif">
			<strong><?php echo $return_title ?></strong><br>
			<?php echo $return_caption ?>
		</fieldset>
	<?php
		}
	?>
    <div id="button">
        <input type="button" value="Cadastrar" name="button" id="button" onClick="getOpen('?pagina=apoiadores&action=inserir','_self')"  />
    </div>
<?php
	}
?>