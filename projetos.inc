<?php

/**
 * @author Heron R. dos Santos
 * @copyright 2008 TelosOnline.info
 */



?>
<head>
	<script type="text/javascript" src="simpletree/simpletreemenu.js">
	
	/***********************************************
	* Simple Tree Menu- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
	* This notice MUST stay intact for legal use
	* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
	***********************************************/
	
	</script>
	
	<link rel="stylesheet" type="text/css" href="simpletree/simpletree.css" />
</head>
<h1>Projetos</h1>

<p>Temos convic��o que o mandato de vereador tem as suas limita��es, mas � por outro lado - pode ser um grande instrumento de transforma��o da nossa sociedade. Listamos abaixo algumas id�ias e propostas, que Edson Cardoso � Edinho, pretende desenvolver quando chegar � C�mara:</p>

<ul id="projetos" class="treeview">
	<li>Exerc�cio do mandato
		<ul>
			<li>Prop�r corte de ponto aos vereadores que faltam �s sess�es sem justificativa. Se um trabalhador falta o servi�o n�o � assim que funciona? Com os vereadores n�o deve ser diferente!</li>
			<li>Criar a campanha �� Lei? Tem que Cumprir!�. Muitas leis de interesse da popula��o j� foram aprovadas, mas n�o �pegaram�. Atrav�s da negocia��o, da imprensa e da justi�a, faremos essas leis serem cumpridas.</li>
			<li>Implantar o Gabinete Itinerante. O Vereador deve ir onde o povo est�, por isso, uma vez por semana nosso vereador estar� em uma localidade diferente atendendo os moradores da regi�o. Ouvindo suas cr�ticas, sugest�es e prestando conta do trabalho na C�mara.</li>
			<li>Reativar o projeto C�mara Itinerante, onde - uma vez por m�s - a sess�o da C�mara acontece em um bairro diferente da cidade.</li>
		</ul>
	</li>
	<li>Meio Ambiente
		<ul>
			<li>Controle da popula��o de cachorros abandonados: o grande n�mero de cachorros nas ruas de Nova Igua�u, acarreta v�rios problemas, pois eles reviram o lixo; fazem suas �necessidades� nas ruas; atacam pedestres, ciclistas e motociclistas e ainda podem transmitir doen�as. Atrav�s de parceria com universidades e o apoio da Prefeitura, iremos criar um programa para esteriliza��o das f�meas e conscientizar os moradores a n�o deixarem filhotes nas ruas.</li>
			<li>Pedalar � Preciso: Apoiar o Movimento pela implanta��o de ciclovias e biciclet�rios em Nova Igua�u.</li>
			<li>Aumentar a coleta seletiva na cidade.</li>
			<li>Prop�r projeto de lei para dar incentivos fiscais a empresas e resid�ncias que utilizarem tecnologias sustent�veis de constru��o (energia solar, e�lica e hidr�ulica, tratamento de esgoto e �guas cinzas, reuso da �gua).</li>
			<li>Incentivar o plantio de �rvores em cal�adas e �reas degradadas.</li>
		</ul>
	</li>
	<li>Turismo
		<ul>
			<li>Liderar a produ��o da segunda edi��o do Guia �Conhe�a Nova Igua�u�. Para difundir entre os moradores e visitantes da cidade o que temos de bom.</li>
			<li>Prop�r a cria��o de uma cooperativa de charretes para atender os visitantes do entorno da Reserva de Tingu�</li>
			<li>Sugerir a sinaliza��o (tur�stica e indicativa) da Serra do Vulc�o</li>
		</ul>
	</li>
	<li>Esporte
		<ul>
			<li>Cria��o de uma vila ol�mpica em cada URG � Unidade Regional de Governo</li>
		</ul>
	</li>
	<li>Cultura
		<ul>
			<li>Cria��o de uma escola p�blica de m�sica</li>
			<li>Apoiar as r�dios comunit�rias</li>
			<li>Implantar o projeto Cinema Itinerante, democratizando o acesso � produ��o audiovisual levando exibi��o de filmes em tel�o para diversos bairros da cidade</li>
		</ul>
	</li>
	<li>Desenvolvimento Comunit�rio
		<ul>
			<li>Apoiar a cria��o e manuten��o de associa��es de moradores</li>
		</ul>
	</li>
	<li>Pessoas com Defici�ncia
		<ul>
			<li>Lutar pela padroniza��o de cal�adas </li>
			<li>Fazer com que leis como a 2861/97, que �determina que as casas de espet�culo e cong�neres (teatros, cinemas e clubes) a adaptar suas instala��es para o livre acesso de pessoas portadoras de defici�ncia, sejam cumpridas�</li>
			<li>Apresentar projeto de lei obrigando a instala��o de sem�foros sonoros, priorizando os seguintes locais: Hospital Geral de Nova Igua�u, Terminal Rodovi�rio da CODERTE, Instituto de Educa��o Rangel Pestana e na Esta��o Ferrovi�ria.</li>
		</ul>
	</li>
	<li>Administra��o P�blica
		<ul>
			<li>Prop�r a cria��o da Secretaria Municipal de Agricultura. O tamanho da produ��o e da �rea rural em nossa cidade justifica a cria��o dessa Secretaria. Os agricultores devem ser tratados com respeito e ter uma boa estrutura para atend�-los</li>
			<li>Sugerir a extin��o da Secretaria Municipal de Participa��o Popular e da Secretaria Adjunta de Articula��o Pol�tica: As atribui��es desse �ltimo �rg�o deve fazer parte da Secretaria de Governo e participa��o popular � uma vertente que deve ser respeitada em todos as �reas da Administra��o P�blica, por isso n�o deve ter uma secretaria</li>
		</ul>
	</li>
	<li>Combate � Homofobia
		<ul>
			<li>Apresentar Projeto de Lei, dando direito aos travestis de serem tratados pelo seu "nome social" nos servi�os p�blicos oferecidos em Nova Igua�u.</li>
		</ul>
	</li>
	<li>Tr�nsito
		<ul>
			<li>Intermediar uma solu��o entre a Ortobom e a Prefeitura para resolver o problema de estacionamento de caminh�es na Est. da Guarita e entorno.</li>
		</ul>
	</li>
</ul>
<p>
	<img src="images/corpo.assinatura.gif">
</p>
<script type="text/javascript">
	//ddtreemenu.createTree(treeid, enablepersist, opt_persist_in_days (default is 1))
	ddtreemenu.createTree("projetos", true, 5)
</script>