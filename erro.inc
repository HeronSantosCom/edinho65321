<?php

/**
 * @author Heron R. dos Santos
 * @copyright 2008 TelosOnline.info
 */



?>
<h1>Erro</h1>
<?php
	$return_type = "erro";
	$return_title = "Erro!";
	$return_caption = "Ocorreu um erro ao acessar esta p�gina...<br>Clique em \"Retornar...\" para prosseguir...";
?>
<fieldset id="box" class="<?php echo $return_type ?>">
	<img class="icon" src="images/box.<?php echo $return_type ?>.gif">
	<strong><?php echo $return_title ?></strong><br>
	<?php echo $return_caption ?>
    <div id="button">
        <input type="button" value="Tentar novamente..." name="button" id="button" onClick="getOpen('?pagina=default','_self')" />
    </div>
</fieldset>