<?php

/**
 * @author Heron R. dos Santos
 * @copyright 2008 TelosOnline.info
 */



?>
<h1>Perfil</h1>
<p>Iniciei minha milit�ncia pol�tica no Gr�mio Estudantil do Col�gio Pedro II. Atuar no Movimento Estudantil foi uma grande escola pra mim. Ap�s essa participa��o, optei por entrar no Movimento Comunit�rio.</p>
<p>Ajudei, juntamente com um grupo de moradores, a reativar a AMATRECO (Associa��o de Moradores do Bairro Tr�s Cora��es) e a partir da� constru� uma intensa rela��o com Nova Igua�u.</p>
<p>� frente da AMATRECO pude fazer um bom trabalho e me credenciei a concorrer uma vaga � C�mara de Vereadores. Durante os quatro (4) anos que dirigi a referida Associa��o, implantei projeto de Gari Comunit�rio, adquiri computadores e consegui obras de saneamento, pavimenta��o e reurbaniza��o para o bairro.</p>
<p>Al�m de reativar esta Associa��o, tive papel de destaque na retomada do Gr�mio Recreativo Escola de Samba Unidos de Tr�s Cora��es, que hoje possui a melhor e maior quadra de samba de Nova Igua�u.</p>
<p>Estou construindo a responsabilidade de me preocupar com os grandes temas da cidade e do mundo (aquecimento global, desenvolvimento urbano, planejamento ciclo vi�rio, etc...), sem esquecer os problemas que atingem diretamente os moradores de Nova Igua�u, como ilumina��o, saneamento e �reas de lazer.</p>
<p>Conto com voc� no desafio de construir um mandato em prol do desenvolvimento de nossa cidade.</p>
<p><strong>Venha conosco!</strong></p>
<p><img src="images/corpo.assinatura.gif"></p>